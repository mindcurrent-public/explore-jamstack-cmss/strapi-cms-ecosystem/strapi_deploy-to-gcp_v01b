# strapi_deploy-to-gcp_v01b

Create a minimal (exploratory/throw-away) Strapi CMS instance running on GCP.

History: This is essentially a clone of the "_v1a" version, but available for public access.

In this project, we will follow: 
* the Strapi (general) [Deployment Guide](https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/deployment.html) 
* and (specific) [Google App Engine Guide](https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/deployment/hosting-guides/google-app-engine.html) 
* and (reference) Strapi [Configurations Guide](https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html)

...to create a Strapi CMS instance running on GCP.

Based on previous experiences w/ GCP this will likely turns out NOT to be as straight-forward as it might initially seem.

Details of the saga are tracked in the issues, in a GSD manner.

---

### Currently Deployments:
* Strapi CMS server (on GCP): 
  * Base Server: https://mc-strapi-on-gcp-v1-dot-mc-ai-rest-service-v03.wm.r.appspot.com
  * Admin Portal: https://mc-strapi-on-gcp-v1-dot-mc-ai-rest-service-v03.wm.r.appspot.com/admin/
  * GraphQL API (NOT FUNCTIONAL): https://mc-strapi-on-gcp-v1-dot-mc-ai-rest-service-v03.wm.r.appspot.com/graphql
  * REST API Example Endpoints:  
    * FORBIDDEN: https://mc-strapi-on-gcp-v1-dot-mc-ai-rest-service-v03.wm.r.appspot.com/users


---

### MVP process "Punch-List"

1. [ ] Create a new (standalone) generic Strapi project
1. [ ] Perform initial project git-RCS commits
1. [ ] Install and initialize the Google Cloud SDK (if not yet done)
1. [ ] Create config files required for GCP: app.yaml and .gcloudignore
1. [ ] Configure the (PostgreSQL on GCP) database
1. [ ] Configure Auto-Build (on GCP) after deploy
1. [ ] Deploy the Strapi App onto GCP

For details, look to the **[closed issues](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/strapi-cms-ecosystem/strapi_deploy-to-gcp_v01b/-/issues?scope=all&state=closed)**.

---

### To-Dos: Look to the **[open issues](https://gitlab.com/mindcurrent-public/explore-jamstack-cmss/strapi-cms-ecosystem/strapi_deploy-to-gcp_v01b/-/issues)**.
